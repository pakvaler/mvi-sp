%% 
%% Created in 2018 by Martin Slapak
%%
%% Based on file for NRP report LaTeX class by Vit Zyka (2008)
%%
%% Compilation:
%% >pdflatex report
%% >bibtex report
%% >pdflatex report
%% >pdflatex report

\documentclass[english]{mvi-report}

\usepackage[utf8]{inputenc} 
\usepackage{listings}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{float}
\usepackage{subfig}

\lstset{frame=tb,
  language=Python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{lightgray},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=2
}

\title{Vehicle color prediction}

\author{Valeriya Pak}
\affiliation{CTU - FIT}
\email{pakvaler@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
At our company we've collected a large set of car images as a medium to tackle different image processing problems. And one of the tasks I got assigned to is to train a model that would predict vehicle color.

% \ldots


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dataset}

The data came from car trading platforms like \hyperref{https://sauto.cz}{}{}{sauto.cz}, thus color information was available for a significant amount of images.

As a preprocessing step, I run an object detection model on the raw data and took only foreground cars to reduce noise in the data. For the school task purpose, I was allowed to take a random sample of 20000 images as my dataset.

The dataset consists of 8 unbalanced colors where black and silver prevail \ref{fig:groupbycolor}.

\begin{figure}[h]
  \centering
  \leavevmode
  \includegraphics[width=1\linewidth]{img/groupbycolor.png}
  \vskip-0.5cm
  \caption{color distribution}
  \label{fig:groupbycolor}
\end{figure}

I split the whole dataset into train/test with 1:3 ratio. To reduce the impact of unbalancedness I also estimated class weights using "balanced" heuristic from sklearn package \cite{compute_class_weight}.
This will be discussed later in section \ref{balance}.

\begin{lstlisting}[caption={“balanced” heuristic usage}]
from sklearn.utils import class_weight
classes = df['color'].unique()
balanced_class_weights = class_weight.compute_class_weight('balanced', classes=classes, y=df['color'])
\end{lstlisting}

% Původ, proces získání, předzpracování, \ldots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Model selection}
% Použité metody, jejich přizpůsobení, aplikace\ldots
For the sack of simplicity I selected 4 widely known network architectures from \hyperref{https://keras.io/api/applications/}{}{}{keras models}:

\begin{itemize}
  \item Resnet50
  \item VGG16
  \item Xception
  \item InceptionV3
\end{itemize}

I also wrote simple wrappers above every architecture so it can train either from scratch or from imagenet-trained weights (discussed below). 
The wrappers also return appropriate preprocessing functions.

\begin{lstlisting}[caption={Wrapper example}]
from tensorflow.keras.applications import ResNet50
from tensorflow.keras.applications.resnet50 import preprocess_input as preprocess_input_resnet

def resnet(image_size, loss='categorical_crossentropy', weights='imagenet', pooling='avg'):
base_model = ResNet50(weights=weights, include_top=False, input_shape=(image_size, image_size, 3), pooling=pooling)
if weights is not None:
    for layer in base_model.layers:
        layer.trainable = False # unfreezing later depending on stategy
x = layers.Flatten()(base_model.output) if pooling is None else base_model.output
if 'hinge' in str(loss):
    x = Dense(len(classes), activation='tanh')(x)
else:
    x = Dense(len(classes), activation='softmax')(x)
name = 'resnet--' + ('from-scratch' if weights is None else 'pretrained')
return Model(inputs=base_model.input, outputs=x, name=name), preprocess_input_resnet, None
\end{lstlisting}

\section{Training approach}
I followed a quadrant rule to select the most useful training strategy\cite{transfer_learning}:

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=0.9\linewidth]{img/strategies.png}\vskip-0.5cm
  \caption{Fine-tuning strategies}
  \label{fig:transfer_learning_strategies}
\end{figure}

\begin{figure}[H]
  \centering\leavevmode
  \subfloat{\includegraphics[width=0.45\linewidth]{img/approach.png}}
  \subfloat{\includegraphics[width=0.45\linewidth]{img/approach2.png}}
  \vskip-0.5cm
  \caption{Size-Similarity matrix and decision map for fine-tuning pre-trained models}
  \label{fig:transfer_learning_quadants}
\end{figure}

My dataset consisted of 20,000 images and I thought it may be enough to train smaller model from scratch, since the classification problem seemed to be relatively easy. 
But I also thought that the color information may be present in features learnt from imagenet.
So for the sake of completeness, I decided to evaluate all 3 approaches (training from scratch, fine-tuning and transfer learning) and select the best performing one, more in section \ref{strategy}. 

\section{Visualizations and preliminary run}
No wonder, pre-trained models seem to converge way faster probably because they already have color information present in their feature space, whereas randomly initialized models need more accurate hyperparameter settings and longer training to learn it.

The xception architecture has quite promising results in both pretrained and from-scratch scenarios (having the loss ~0.5 and accuracy ~0.86 for with pretrained weights), so I chose  \textbf{xception as the baseline model}.

Interestingly, VGG16 doesn't work in both scenarios. A possible reason is that the optimizer can't guess a suitable learning rate in just 3 epochs.
Longer training may be required here. However, due to the fact that VGG16 has the worst performance/\#params ratio on the ImageNet dataset, I will not analyze it any further.

I used Facebook HiPlot to analyze model performance.
It's a handy tool to visualize multidimensional data and explore parameter correlations.
Note that uid and from\_uid below are utility parameters needed for HiPlot and have no specific meaning for the analysis.

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/hiplot1.png}
  \caption{Preliminary run results - \href{https://pages.fit.cvut.cz/pakvaler/mvi-sp/hiplot1.html}{interactive}}
  \label{fig:hiplot_preliminary}
\end{figure}

\section{Select loss function}
I decided to test how different loss functions affect model performance.
I decided to compare classic categorical crossentropy against "maximum-margin" hinge loss. A was also advised to try so-called label smoothing extension \cite{smoothing} to the categorical crossentropy loss, which should prevent network from giving over-confident predictions and help it better tolerate noise in the data.

So here I test 3 types of loss functions:
\begin{itemize}
  \item hinge loss (SVM-like loss)
  \item categorical crossentropy loss
  \item categorical crossentropy with label smoothing
\end{itemize}

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/hiplot2.png}
  \caption{Loss impact - \href{https://pages.fit.cvut.cz/pakvaler/mvi-sp/hiplot2.html}{interactive}}
  \label{fig:hiplot_loss}
\end{figure}


It is obvious that the hinge loss has the least performance. Perhaps, this is because the original network learned a different loss landscape and now it requires some time to maximize margins between the classes.
On the other hand, label smoothing didn't help the network to improve it's performance. Even though the overall loss was higher, the network became less prone to overfitting (difference between testing validation losses was smaller).
From this point I'll be using categorical crossentropy with label smoothing as a baseline.

\section{Reducing image resolution}
Color classification seems to me as a simple enough task that does not require one to look at HD images to make a prediction, so I decided to try to reduce the model's input resolution and speedup prediction time.
I analyzed the smallest resolution that the xception can handle (71x71), the doubled resolution (150x150) and a standard 224x224 one.
Unfortunately, changing image resolution doesn't affect models capacity, so this will not lead to the improvement in terms of prediction power.

\begin{figure}[H]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/hiplot3.png}
  \caption{Image resoluiton impact - \href{https://pages.fit.cvut.cz/pakvaler/mvi-sp/hiplot3.html}{interactive}}
  \label{fig:hiplot_resolution}
\end{figure}

Based on the graph above, 71x71 resolution seems to be a bit insufficient. On the other hand, 150x150 and 224x224 sizes have comparable performance. On the other hand, training times differ significantly. For instance, one running single epoch on 71x71 images is almost 10 times faster. This provides a handy tradeoff between accuracy and throughput. 
However, I'll be using a full size models further to achieve the best performance possible.

\section{Select optimizer}
In this part I run slightly longer training (8 epoch vs 6 epochs) for every architecture with a set of optimizers and checked the resulting loss and convergence rates.
Since learning rate is dependent on the batch size, I set it to the largest possible value and then optimize the learning rate. 

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/hiplot4.png}
  \caption{LR optimizer - \href{https://pages.fit.cvut.cz/pakvaler/mvi-sp/hiplot4.html}{interactive}}
  \label{fig:hiplot_optimizer}
\end{figure}

According to the results, the most suitable optimizer is SGD either with simple learning rate 0.01 or 0.006 with Nesterov momentum. I'll use simple configuration in future experiments.

\section{Test class weighting for unbalanced classes}\label{balance}
My dataset isn't extremly unbalanced, however, some balancing techniques still worth to try. I decided to test built-in keras capability to weight different classes when computing the loss.
This time I will also use confusion matrix to measure the impact of class weighting since the losses won't be comparable.

\begin{figure}[H]
  \centering\leavevmode
  \subfloat{\includegraphics[width=0.5\linewidth]{img/matrix1.png}}
  \subfloat{\includegraphics[width=0.5\linewidth]{img/matrix2.png}}
  \caption{Confusion matrix for unbalanced (left) and balanced (right) class weights}
  \label{fig:confusion_matrix}
\end{figure}

However, I still monitor loss to save the best checkpoint within each configuration.

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/hiplot5.png}
  \caption{Class weights balancing and accuracy - \href{https://pages.fit.cvut.cz/pakvaler/mvi-sp/hiplot5.html}{interactive}}
  \label{fig:hiplot_balance}
\end{figure}

Class weighting seems to have negative impact on performance for minority classes even if it has slightly better accuracy in general. I decided not to use this setting in the following sections.

\section{Select training strategy}\label{strategy}
I tested the following startegies:
\begin{itemize}
  \item network from scratch
  \item style-transfer on pre-trained network (training only classifier layer)
  \item fine-tuning upper half of the network
  \item fine-tuning all but first layers of the network
\end{itemize}

For pre-trained models the more layers are being trained the better the result. 
Unfreezing only the last layer seems to be insufficient and requires more deep training. 
Model with randomly initialized weights still needs more time to learn features and will be analyzed in the next section.

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/hiplot6.png}
  \caption{Gradually unfreezing network layers - \href{https://pages.fit.cvut.cz/pakvaler/mvi-sp/hiplot6.html}{interactive}}
  \label{fig:hiplot_unfreezing}
\end{figure}

\section{Select best performing configuration}
Here I tested several well-performing configurations for more epochs.
I set checkpoint saving to optimize validation loss to later analyze best configuration more thorougly.

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/hiplot7.png}
  \caption{Select best performing configuration - \href{https://pages.fit.cvut.cz/pakvaler/mvi-sp/hiplot7.html}{interactive}}
  \label{fig:hiplot_epochs}
\end{figure}

According to the visualization \ref{fig:hiplot_epochs}, the network trained from scratch managed to learn good features, but it was still outperformed by the pre-trained one and got overfitted in the end. So I can conclude that using pre-trained features is beneficial in general case and that 20000 images is a sufficiently large dataset to get good results in car color prediction.

\section{Performance evaluation}
Since the data are unbalanced, I used confusion matrix to evaluate pretrained model performance for different classes.
Then I performed empirical check on incorrectly classified images to assess the number of real mistakes.

\begin{lstlisting}[caption={Run predictions}]
# Use the best configuration
loss_f = CategoricalCrossentropy(label_smoothing=.1)
optimizer = lambda: optimizers.SGD(lr=.01)
ground_truth, predictions, label2index = predict_at_checkpoint(lambda: xception(image_size=224, loss=loss_f, weights='imagenet'),
                                                               class_weights=None, unfreeze_last=0, optimizer=optimizer(), loss_f=loss_f, batchsize=32)

# Getting the mapping from class index to class label
idx2label = {v:k for k,v in label2index.items()}
predicted_classes = np.argmax(predictions,axis=1)
argsort_predictions = np.argsort(predictions, axis=1)

errors = np.where(predicted_classes != ground_truth)[0]
print("No of errors = {}/{}".format(len(errors),predictions.shape[0]))
print(f'Final accuracy = {100-len(errors)*100/predictions.shape[0]:.2f}%')

#Loading model from /data/datasets/color/models/xception--pretrained_224x224_balanced-False_unfreeze-0_opt-SGD_lr-0.01_loss-categorical-crossentropy-ls0.1_bs-32
#Found 5000 validated image filenames belonging to 8 classes.
#157/156 [==============================] - 35s 224ms/step
#No of errors = 611/5000
#Final accuracy = 87.78%
\end{lstlisting}

\begin{figure}[H]
  \centering\leavevmode
  \includegraphics[width=0.9\linewidth]{img/matrix3.png}
  \caption{Final model confusion matrix}
  \label{fig:confusion_matrix_fin}
\end{figure}


Based on the empirical evaluation of incorrectly classified images, I can make several conclusions:
\begin{itemize}
  \item Color classification problem is highly dependent on the lighting conditions, i.e. when the image is overexposed/underexposed or when it's too sunny. It may be hard to determine vehicle color (i.e. white vs. beige vs. silver or dark blue vs. black) from a single image even for humans \ref{fig:blue_black}. For best performance, one should consider averaging predictions from several view angles.
  \item The data are in fact quite noisy. Some pictures clearly belong to the different class than it is supposed to be, which is often given by errors in the bulk advertisements \ref{fig:red_black} or presence of commercial ads.
  \item Despite the level of noise, models can clearly tolerate it and give correct predictions even for wrongly labelled pictures. The actual model performance may thus be even higher.
\end{itemize}

\begin{figure}[H]
  \centering\leavevmode
  \subfloat[Ground Truth: blue; Prediction: black 48.9\%]{\includegraphics[width=0.45\linewidth]{img/blue_black.png}}
  \subfloat[Ground Truth: beige cream; Prediction: white 61.7\%]{\includegraphics[width=0.45\linewidth]{img/cream_white.png}}
  \caption{Besause the perception of color is subjective, sometimes it is hard to determine whether Ground Truth or prediction is \textit{more correct} }
  \label{fig:blue_black}
\end{figure}


\begin{figure}[H]
  \centering\leavevmode
  \subfloat[Ground Truth: silver grey; Prediction: beige cream 43.8\%]{\includegraphics[width=0.45\linewidth]{img/grey_cream.png}}
  \subfloat[Ground Truth: blue; Prediction: silver grey 76.0\%]{\includegraphics[width=0.45\linewidth]{img/blue_grey.png}}
  \caption{Besause the perception of color is subjective, sometimes it is hard to determine whether Ground Truth or prediction is \textit{more correct} }
  \label{fig:aaa}
\end{figure}


\begin{figure}[H]
  \centering\leavevmode
  \includegraphics[width=0.4\linewidth]{img/red_black.png}
  \caption{Advertisement: Ground Truth: red; Prediction: black 62.0\%}
  \label{fig:red_black}
\end{figure}


\section{Conclusion}
During the analysis I preprocessed our existing dataset to extract the foreground vehicle exteriors and compared several common network architectures to select the best performing one and then optimized it's hyperparameters. Then, I tested if a set of 20 thousand images is enough to train a model with compititive performance from scratch or is it enough to use pretrained networks as feature extractors and train a simple classifier on top of the provided features. In addition, I checked if standard image resolution can be safely reduced to speedup predictions. I also analyzed the impact of class weighting on the imbalanced problem and assessed the model performance using confusion matrix and empirical comparison. In the end, I investigated the origin of noise in the data.

The result of the analysis was creation of a sufficiently well working model, which we plan to use in production. There are still some things that I didn't have time to experiment with (i.e. data augmentation and different types of pooling), so I consider this a direction for further improvement.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Bibliography
%\bibliographystyle{plain-cz-online}
\bibliography{reference}

\end{document}
