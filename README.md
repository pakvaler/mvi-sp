# mvi-sp
At our company we've collected a large set of car images as a medium to tackle different image processing problems. And one of the tasks I got assigned to is to train a model that would predict vehicle color.

Since the data came mostly from car trading platforms, color information was available for a significant amount of images.
